= Program

Place your program code in this directory.

== Example

.app.py
[source,python]
----
From flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world:
    return 'Hello, world!'


if __name == '__main__':
    app.run(debug=True, host='0.0.0.0')
----

.requirements.txt
[source]
----
Flask==1.1.2
----
